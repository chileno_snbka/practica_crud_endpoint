import {http} from './config'

export default{
    listar:() => {
        return http.get('burguer')
    },

    guardar:(burguer)=>{
        return http.post('burguer', burguer)
    },

    actualizar:(burguer)=>{
        return http.put('burguer/' + burguer._id, burguer)
    },

    borrar:(burguer)=>{
        return http.delete('burguer/' + burguer._id, burguer)
    }
}